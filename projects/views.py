from django.shortcuts import get_object_or_404, redirect, render
from projects.forms import ProjectForm

from tasks.models import Task
from .models import Project
from django.contrib.auth.decorators import login_required


@login_required
def project_list(request):
    projects = Project.objects.filter(owner=request.user)
    return render(
        request, "projects/project_list.html", {"projects": projects}
    )


def show_project(request, id):
    project = get_object_or_404(Project, id=id)
    tasks = Task.objects.filter(project=project)
    context = {"project": project, "tasks": tasks}
    return render(request, "projects/project_detail.html", context)


@login_required
def create_project(request):
    if request.method == "POST":
        form = ProjectForm(request.POST)
        if form.is_valid():
            project = form.save(commit=False)
            project.owner = request.user
            project.save()
            return redirect("list_projects")
    else:
        form = ProjectForm()

    return render(request, "projects/create_project.html", {"form": form})
