from django.urls import path
from projects import views

urlpatterns = [
    path("", views.project_list, name="list_projects"),
    path("projects/<int:id>/", views.show_project, name="show_project"),
    path("create/", views.create_project, name="create_project"),
]
